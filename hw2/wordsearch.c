/* Homework 2, 600.120 Spring 2015
Feb. 16
Phillip Espinoza
phil.espinoza@jhu.edu
pespino5

202-251-5121

*/

#include "wordsearch.h"
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.
int load_grid(FILE* infile, char grid[][MAXSZ]) {

   // reads square grid from infile until first empty line
   // returns the number of rows in the grid or 0 for an error
   // int load_grid(FILE* infile, char outgrid[][MAXSZ]);
  int firstcols = 0;
  int currentcols = 0;
  int rows = 0;
  int i = 0;
  int j = 0;

  // make c int to handle isalpha() and EOF
  int c;

  while((c = fgetc(infile)) != EOF) {


    if(isalpha(c)) {

      grid[i][j++] = (char) c;

      // count each column separately from first 
      if(i > 0) {
	currentcols++;
      }

      // save first column apart from others for checking
      else if(i == 0) {
	firstcols++;
      }
    }


    // begin next row on newline
    else if(c == '\n') {

      // check column consistency
      if(i > 0 && currentcols != firstcols) {
	fputs("Error: column length mismatch.\n", stderr);
	return 0;
      }

      // reset if no mismatch
      else {
	i++;
	j = 0;
	rows++;
	currentcols = 0;
      }
    }

    // check for non-char input
    else if(c != ' ' && c != '\n' && !isalpha(c)) {
      fputs("Error: invalid grid element.\n", stderr);
      return 0;
    }

   // check for invalid size before restarting loop
    if(firstcols == 10 || currentcols == 10) {
      fputs("Error: grid too large.\n", stderr);
      return 0;
    }


  }

  // at end of grid, check for square dimensions
  if (firstcols != rows) {
    fputs("Error: non-square grid.\n", stderr);
    return 0;
  }

  return rows;

}


bool find_word_up(char grid[][MAXSZ], int n, char w[], int i, int j) {

  // check for bad indices
  if((n - 1) < i || (n - 1) < j) {
    return false;
  }

  int gridspace = i + 1;
  int wlen = strlen(w);

  // check for bad length for given grid location or in general
  if(gridspace < wlen || 10 < wlen) {
    return false;
  }

  // look for match
  bool match = true;
  int wcheck = 0;

  // loop through grid checking for char equality
  while((i > -1) && (wcheck < wlen) && match) {

    if(w[wcheck] != grid[i][j]) {
      match = false;
      break;
    }
    wcheck++;
    i--;
  }

  return match;

}


bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j) {


  // check for bad indices
  if((n - 1) < i || (n - 1) < j) {
    return false;
  }

  int gridspace = n - i;
  int wlen = strlen(w);

  // check for bad length for given grid location or in general
  if(gridspace < wlen || 10 < wlen) {
    return false;
  }


  // look for match
  bool match = true;
  int wcheck = 0;


  // loop through grid checking for char equality
  while((i < n) && (wcheck < wlen) && match) {

    if(w[wcheck] != grid[i][j]) {
      match = false;
      break;
    }
    wcheck++;
    i++;

  }

  return match;

}

bool find_word_left(char grid[][MAXSZ], int n, char w[], int i, int j) {

  // check for bad indices
  if((n - 1) < i || (n - 1) < j) {
    return false;
  }

  int gridspace = j + 1;
  int wlen = strlen(w);

  // check for bad length for given grid location or in general
  if(gridspace < wlen || 10 < wlen) {
    return false;
  }

  // look for match
  bool match = true;
  int wcheck = 0;

  // loop through grid checking for char equality
  while((j > -1) && (wcheck < wlen) && match) {

    if(w[wcheck] != grid[i][j]) {
      match = false;
      break;
    }
    wcheck++;
    j--;
  }

  return match;

}

bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j) {
  
  // check for bad indices
  if((n - 1) < i || (n - 1) < j) {
    return false;
  }

  int gridspace = n - j;
  int wlen = strlen(w);

  // check for bad length for given grid location or in general
  if(gridspace < wlen || 10 < wlen) {
    return false;
  }

  // look for match
  bool match = true;
  int wcheck = 0;

  // loop through grid checking for char equality
  while((j < n) && (wcheck < wlen) && match) {
    if(w[wcheck] != grid[i][j]) {
      match = false;
      break;
    }
    wcheck++;
    j++;
  }

  return match;

}

void write_all_matches(FILE* outfile, char grid[][MAXSZ], int n, char w[]) {

  bool notfound = true;

  for(int i = 0; i < n; i++) {
    for(int j = 0; j < n; j++) {
      
      if(find_word_up(grid, n, w, i, j)){
	fprintf(outfile, "%s %u %u U\n", w, i, j);
	notfound = false;
      }
      
      if(find_word_down(grid, n, w, i, j)){
	fprintf(outfile, "%s %u %u D\n", w, i, j);
	notfound = false;
      }
      
      if(find_word_left(grid, n, w, i, j)){
	fprintf(outfile, "%s %u %u L\n", w, i, j);
	notfound = false;
      }
      
      if(find_word_right(grid, n, w, i, j)){
	fprintf(outfile, "%s %u %u R\n", w, i, j);
	notfound = false;
      }

    }
  }

  if(notfound) {
    fprintf(outfile, "%s - Not Found.\n", w);
    }

}
