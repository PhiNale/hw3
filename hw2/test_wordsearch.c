/* Homework 2, 600.120 Spring 2015
Feb. 16
Phillip Espinoza
phil.espinoza@jhu.edu
pespino5

202-251-5121

*/

#include "wordsearch.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match.
 */
bool fileeq(char lhsName[], char rhsName[]) {
    FILE* lhs = fopen(lhsName, "r");
    FILE* rhs = fopen(rhsName, "r");

    // don't compare if we can't open the files
    if (!lhs || !rhs) return false;

    bool match = true;
    // read until both of the files are done or there is a mismatch
    while (!feof(lhs) || !feof(rhs)) {
        if (feof(lhs) ||                  // lhs done first
            feof(rhs) ||                  // rhs done first
            (fgetc(lhs) != fgetc(rhs))) { // chars don't match
            match = false;
            break;
        }
    }
    fclose(lhs);
    fclose(rhs);
    return match;
}

/*
 * Test fileeq on same file, files with same contents, files with
 * different contents and a file that doesn't exist
 */
void test_fileeq() {
    FILE* fptr = fopen("test1.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    fptr = fopen("test2.txt", "w");
    fprintf(fptr, "this\nis\na different test\n");
    fclose(fptr);

    fptr = fopen("test3.txt", "w");
    fprintf(fptr, "this\nis\na test\n");
    fclose(fptr);

    assert(fileeq("test1.txt", "test1.txt"));
    assert(fileeq("test2.txt", "test2.txt"));
    assert(!fileeq("test2.txt", "test1.txt"));
    assert(!fileeq("test1.txt", "test2.txt"));
    assert(fileeq("test3.txt", "test3.txt"));
    assert(fileeq("test1.txt", "test3.txt"));
    assert(fileeq("test3.txt", "test1.txt"));
    assert(!fileeq("test2.txt", "test3.txt"));
    assert(!fileeq("test3.txt", "test2.txt"));
    assert(!fileeq("", ""));  // can't open file
}


void testfile(char c) {

  FILE* out;

  switch(c) {
  case 'u':
    out = fopen("upmatch.txt", "w");
    fputs("m e g x\n", out);
    fputs("l g i v\n", out);
    fputs("i o p m\n", out);
    fputs("f d s r\n", out);
    fclose(out);
    break;
  case 'd':
    out = fopen("downmatch.txt", "w");
    fputs("f d s r\n", out);
    fputs("i o p m\n", out);
    fputs("l g i v\n", out);
    fputs("m e g x\n", out);
    fclose(out);
    break;
  case 'l':
    out = fopen("leftmatch.txt", "w");
    fputs("m l i f\n", out);
    fputs("e g o d\n", out);
    fputs("g i p s\n", out);
    fputs("x v m r\n", out);
    fclose(out);
    break;
  case 'r':
    out = fopen("rightmatch.txt", "w");
    fputs("f i l m\n", out);
    fputs("do g e\n", out);
    fputs("s p  i g\n", out);
    fputs("v m v x\n", out);
    fclose(out);
    break;
  }

}

// generate randomly populated grid of given size to "populated.txt"
// for use with test_read_grid()
// specify rows and columns for grid
// toggle allows choice between isalpha() chars only or mix of isalpha() and !isalpha()

void populate(int rows, int columns, int toggle) {

  FILE* file = fopen("populated.txt", "w");
  char c;

  // populate grid with only alpha chars
  if(toggle == 0) {

    for(int i = 0; i < rows; i++) {
      for(int j = 0; j < columns; j++) {
	c = 'a' + (rand() % 26);
	fputc(c, file);
	fputc(' ', file);
      }
      fputc(10, file); // insert newline
    }

    fclose(file);
  }

  // populate grid with alpha and nonalpha chars
  else if(toggle == 1) {
    int charswitch;
    for(int i = 0; i < rows; i++) {
      for(int j = 0; j < columns; j++) {
	charswitch = rand() % 2;

	if(charswitch == 0) {
	  c = 'a' + (rand() %26);
	  fputc(c, file);
	  fputc(' ', file);
	}
	else if (charswitch == 1) {
	  c = '!' + (rand() %26);
	  fputc(c, file);
	  fputc(' ', file);	  
	}

      }
      fputc(10, file); // insert newline
    }

    fclose(file);
  }

}

/*
 * writes out a hard-coded grid to a file;
 * uses read_grid to read in the grid;
 * test that the loaded grid is correct
 n */
void test_read_grid() {
  char grid[MAXSZ][MAXSZ];
  int loadcheck;

  // check correct grid
  populate(5, 5, 0);
  FILE* correct = fopen("populated.txt", "r");
  loadcheck = load_grid(correct, grid);
  assert(loadcheck != 0);
  fclose(correct);
  
  // check tall grid
  populate(5, 2, 0);
  FILE* tall = fopen("populated.txt", "r");
  loadcheck = load_grid(tall, grid);
  assert(loadcheck == 0);
  fclose(tall);

  // check wide grid
  populate(2, 5, 0);
  FILE* wide = fopen("populated.txt", "r");
  loadcheck = load_grid(wide, grid);
  assert(loadcheck == 0);
  fclose(wide);
  
  // check big grid
  populate(20, 50, 0);
  FILE* big = fopen("populated.txt", "r");
  loadcheck = load_grid(big, grid);
  assert(loadcheck == 0);
  fclose(big);

  // check for non-chars
  populate(3, 3, 1);
  FILE* mixed = fopen("populated.txt", "r");
  loadcheck = load_grid(mixed, grid);
  assert(loadcheck == 0);
  fclose(mixed); 


}

void test_find_forward() {  //TODO 

  char grid[MAXSZ][MAXSZ];
  FILE* file = fopen("rightmatch.txt", "r");
  int load = load_grid(file, grid);

  // ensure load functioned properly
  assert(load != 0);

  // check for correct matches, with various lengths, locations, whitespace
  bool pig = find_word_right(grid, load, "pig", 2, 1);
  bool film = find_word_right(grid, load, "film", 0, 0);
  bool dog = find_word_right(grid, load, "dog", 1, 0);

  // check for non-existent matches, bad locations, bad lengths
  bool ant = find_word_right(grid, load, "ant", 2, 1);
  bool bee = find_word_right(grid, load, "bee", 20, 300);
  bool tropicalstorm = find_word_right(grid, load, "tropicalstorm", 0, 0);

  assert(pig);
  assert(film);
  assert(dog);

  assert(!ant);
  assert(!bee);
  assert(!tropicalstorm);

  fclose(file);

}
void test_find_backward() { //TODO 
  char grid[MAXSZ][MAXSZ];
  FILE* file = fopen("leftmatch.txt", "r");
  int load = load_grid(file, grid);

  // ensure load functioned properly
  assert(load != 0);

  // check for correct matches, with various lengths, locations, whitespace
  bool pig = find_word_left(grid, load, "pig", 2, 2);
  bool film = find_word_left(grid, load, "film", 0, 3);
  bool dog = find_word_left(grid, load, "dog", 1, 3);

  // check for non-existent matches, bad locations, bad lengths
  bool ant = find_word_left(grid, load, "ant", 2, 1);
  bool bee = find_word_left(grid, load, "bee", 20, 300);
  bool tropicalstorm = find_word_left(grid, load, "tropicalstorm", 0, 0);

  assert(pig);
  assert(film);
  assert(dog);

  assert(!ant);
  assert(!bee);
  assert(!tropicalstorm);

  fclose(file);

}
void test_find_up() { //TODO 
  char grid[MAXSZ][MAXSZ];
  FILE* file = fopen("upmatch.txt", "r");
  int load = load_grid(file, grid);

  // ensure load functioned properly
  assert(load != 0);

  // check for correct matches, with various lengths, locations, whitespace
  bool pig = find_word_up(grid, load, "pig", 2, 2);
  bool film = find_word_up(grid, load, "film", 3, 0);
  bool dog = find_word_up(grid, load, "dog", 3, 1);

  // check for non-existent matches, bad locations, bad lengths
  bool ant = find_word_up(grid, load, "ant", 2, 1);
  bool bee = find_word_up(grid, load, "bee", 20, 300);
  bool tropicalstorm = find_word_up(grid, load, "tropicalstorm", 0, 0);

  assert(pig);
  assert(film);
  assert(dog);

  assert(!ant);
  assert(!bee);
  assert(!tropicalstorm);

  fclose(file);

}
void test_find_down() { //TODO 
  char grid[MAXSZ][MAXSZ];
  FILE* file = fopen("downmatch.txt", "r");
  int load = load_grid(file, grid);

  // ensure load functioned properly
  assert(load != 0);

  // check for correct matches, with various lengths, locations, whitespace
  bool pig = find_word_down(grid, load, "pig", 1, 2);
  bool film = find_word_down(grid, load, "film", 0, 0);
  bool dog = find_word_down(grid, load, "dog", 0, 1);

  // check for non-existent matches, bad locations, bad lengths
  bool ant = find_word_down(grid, load, "ant", 2, 1);
  bool bee = find_word_down(grid, load, "bee", 20, 300);
  bool tropicalstorm = find_word_down(grid, load, "tropicalstorm", 0, 0);

  assert(pig);
  assert(film);
  assert(dog);

  assert(!ant);
  assert(!bee);
  assert(!tropicalstorm);

  fclose(file);

}
void test_write_all_matches() { //TODO
  
  char grid[MAXSZ][MAXSZ];
  int loadcheck;
 
  FILE* file = fopen("allmatch.txt", "r");
  loadcheck = load_grid(file, grid);  
  assert(loadcheck == 4); 


  write_all_matches(stdout, grid, 4, "farm");
  write_all_matches(stdout, grid, 4, "ant");
  write_all_matches(stdout, grid, 4, "pal");
  write_all_matches(stdout, grid, 4, "hats");

  fclose(file);

}


int main(void) {
    printf("Testing fileeq...\n");
    test_fileeq();
    printf("Passed fileeq tests.\n");

    testfile('u');
    testfile('d');
    testfile('l');
    testfile('r');

    printf("Running wordsearch tests...\n");
    test_read_grid();
    test_find_forward();
    test_find_backward();
    test_find_up();
    test_find_down();
    test_write_all_matches();
    printf("Passed wordsearch tests.\n");
}
