/* Homework 2, 600.120 Spring 2015
Feb. 16
Phillip Espinoza
phil.espinoza@jhu.edu
pespino5

202-251-5121

*/

#include "wordsearch.h"

// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

int main(int argc, char* argv[]) {

  FILE* file = fopen(argv[1], "r");
  char grid[MAXSZ][MAXSZ];

  if(argc < 1) {
    return 1;
  }

  int rows = load_grid(file, grid);

  if(rows == 0) {
    return 1;
  }

  char word[11];

  while(fgets(word, 11, stdin) != NULL) {
    
    write_all_matches(stdout, grid, rows, word);

  }

  return 0;
}
