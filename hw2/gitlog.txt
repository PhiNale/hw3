commit 8cc6f9d42f675809dfe3d9c33f6f7e32ace97b76
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 22:51:27 2015 -0500

    HW2 wrapup

commit 741a23326dba73285b516e5779a13b9637b6bf1f
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 22:40:17 2015 -0500

    HW2 submission preparation - not complete

commit 84954380f342bdcb946aad5eee476052809f1458
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 22:03:06 2015 -0500

    HW2 background done

commit 1bb66e9e4942dd00a7429c3189af7fbcf9acf5c0
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 21:28:26 2015 -0500

    HW2 read left, right working

commit e42efe45c63a06f89bb9f54c3a42f6ce4330f9aa
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 21:05:12 2015 -0500

    HW2 figured out disappearing grid issue

commit 669d80ced5b0f0a4f6f03a90b752002050272aa8
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 19:31:17 2015 -0500

    HW2 find_word_right() done, TSHOOT MSGS IN

commit b08f88f625d2d5e010f76e37615e840c2823f656
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 17:38:35 2015 -0500

    HW2 find_word_right() progress, done?

commit d2adc63547d61865ebe5ec464e619e64be931966
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 02:18:00 2015 -0500

    HW2 more test_read_grid() cleanup

commit 7eb5d8abb198602553fff6172a48bd9d400b7331
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 02:16:58 2015 -0500

    HW2 remove populate() debug comments

commit f237214a4212d0364c239a8134b403f2bcc0eac1
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 02:13:49 2015 -0500

    HW2 remove load_grid() debug comments

commit 36ca2f44f9a9d8adf35ea931b4fd3624b27c14e5
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 02:12:21 2015 -0500

    HW2 populate(), load_grid() working

commit 33a5dec20deb9797b866612196c9d0ca4e29a159
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 01:37:13 2015 -0500

    HW2 load_grid() fixed

commit f2cffd255acf787dd71dd875e3b7c45dac73c788
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 01:08:52 2015 -0500

    HW2 populated() working

commit e96b79a926c491c2ae7a8285b1904db1d4777670
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 00:42:03 2015 -0500

    git practice

commit 46370e2a874ebd115cedbbb564f4673cfe207218
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 16 00:25:03 2015 -0500

    HW2 populate() move, read_grid() work

commit 32b0769b2a561de050d420b62ef3292849302dad
Author: Syv <philespinozaperez@gmail.com>
Date:   Tue Feb 10 17:55:26 2015 -0500

    HW1B - dangling newline sorted

commit 248209e16e8d2d973317d7de41003e52380837d1
Author: Syv <philespinozaperez@gmail.com>
Date:   Tue Feb 10 02:28:07 2015 -0500

    HW1B - gpa() DONE

commit 07472abe5bb4ed6fa947bc5dbef49f064fe14551
Author: Syv <philespinozaperez@gmail.com>
Date:   Tue Feb 10 01:36:16 2015 -0500

    HW1B - gpa() skeleton

commit 3135fcac497db71000374dd61d8e0c5143bc242e
Author: Syv <philespinozaperez@gmail.com>
Date:   Tue Feb 10 00:34:18 2015 -0500

    HW1B q done, n done, gpa() IN PROGRESS

commit dc3764c6b08f8b89c31ae13cd0624929f9e63c31
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 9 23:00:07 2015 -0500

    HW1B switch working?

commit 607e04e96168e1936ed950104196cd9fbf63cff1
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 9 21:16:04 2015 -0500

    HW1A done
    HW1B begun: prompt, fileread done

commit 59bef02ad8d86b83cd767ea691171c8e60112bba
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 9 03:17:30 2015 -0500

    HW1A DONE

commit 036e44c694505504029f41bc4c4ac4b16f1148bb
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 9 02:57:25 2015 -0500

    HW1A binary DONE

commit e6afbfe18296d5adaac9b1c72412c8cd0c20d353
Author: Syv <philespinozaperez@gmail.com>
Date:   Mon Feb 9 02:04:59 2015 -0500

    line-by-line done, tobinary and printing left

commit 376f40b9467d89f0b35e2b88e14595d06e44b3ee
Author: Syv <philespinozaperez@gmail.com>
Date:   Sun Feb 8 19:28:32 2015 -0500

    HW1A binary to char conversion complete

commit 2fde9d753c0b7b3114b2ed9d762667cc9ec8ae74
Author: Syv <philespinozaperez@gmail.com>
Date:   Sun Feb 8 19:05:24 2015 -0500

    HW1A char to int conversion done

commit b4fbbf1b1365db556102532c5a2a0f676919a023
Author: Syv <philespinozaperez@gmail.com>
Date:   Sat Feb 7 20:26:05 2015 -0500

    Homework 1 start, Bitwise practice

commit 414cc671f6844268dedd05d274e4712db17fcdf9
Author: Syv <philespinozaperez@gmail.com>
Date:   Fri Feb 6 17:10:44 2015 -0500

    Adding addition
