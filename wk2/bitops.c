#include <stdio.h>
#include <assert.h>

int main(void) {
  assert((6 & 3) == 2);
  assert((6 | 3) == 7);
  assert((6 ^ 3) == 5);

  printf("All good\n");
  return 0;
}
