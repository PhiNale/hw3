/*
Question 1 pseudocode

  val = 1
    while val < 1000
      print val
      multiply val by 2
*/

#include <stdio.h>

int main (void) {
  /*
  int val = 1;
  while(val < 1000) {
    printf("%d\n", val);
    val *= 2;
    } */

  printf("Question 1\n");
  for (int val = 1; val < 1000; val *= 2) {
    printf("%d\n", val); 
  }

  printf("Question 2\n");
  int array[] = {2, 5, 2, 8, 9, 3, 7, 6, 2, 8, 3};
  int min = 10;
  for(int i = 0; i < 11; i++) {
    if(array[i] < min) {
      min = array[i];
    }
  }
  printf("Minimum = %d\n", min);

  return 0;
}

/*
Question 2 pseudocode:

min - value[0]
  count = 19
  while count > 0
    if (value[count] < min)
      min = value[count]
    decrease count by 1
  print min
*/

/*
Question 3 pseudocode:
nn*/
