/* Homework 0, 600.120 Spring 2015

Feb. 3
Phillip Espinoza
phil.espinoza@jhu.edu
pespino5

202-251-5121


*/

#include <stdio.h>
#include <string.h>

#define LENGTH 16
#define DIVLENGTH 2

int main(int argc, char* argv[])
{
  char course[LENGTH];
  if (argc == 1) {
    printf("Usage: hw0 XX.###.###Gg#.#\n");
    return 1;  // exit program
  }

  strncpy(course, argv[1], LENGTH);  // copy to course string
  course[LENGTH-1] = '\0';   // make sure last character is null
  printf("course string: %s\n", course);  // echo input

  // add your code here

  // data storage variables
  char divcode[] = "XX";
  char dept[] = "XXX";
  char coursenum[] = "XXX";
  char grade = 'X';
  char grademod = 'X';
  char creditval = 'X';
  char creditdecimal = 'X';
  int divnum, gradenum, grademodnum,creddecimal;

  // first 3 course department code (switch)
  strncpy(divcode, course, DIVLENGTH);
  divcode[DIVLENGTH+1] = '\0';

  if (strcmp(divcode, "AS") == 0) {
    divnum = 0;
  }
  else if (strcmp(divcode, "BU") == 0) {
    divnum = 1;
  }
  else if (strcmp(divcode, "ED") == 0) {
    divnum = 2;
  }
  else if (strcmp(divcode, "EN") == 0) {
    divnum = 3;
  }
  else if (strcmp(divcode, "ME") == 0) {
    divnum = 4;
  }
  else if (strcmp(divcode, "PH") == 0) {
    divnum = 5;
  }
  else if (strcmp(divcode, "PY") == 0) {
    divnum = 6;
  }
  else if (strcmp(divcode, "SA") == 0) {
    divnum = 7;
  }
  else {
    printf("Invalid division code.\n");
    return 1;
  }

  // second 3 course number (no operation)
  dept[0] = course[3];
  dept[1] = course[4];
  dept[2] = course[5];

  // course number
  coursenum[0] = course[7];
  coursenum[1] = course[8];
  coursenum[2] = course[9];

  // integer equivalent grade values
  grade = course[10];
  grademod = course[11];

  switch(grade) {
  case 'A': gradenum = 0;
    break;
  case 'B': gradenum = 1;
    break;
  case 'C': gradenum = 2;
    break;
  case 'D': gradenum = 3;
    break;
  case 'F': gradenum = 4;
    break;
  case 'I': gradenum = 5;
    break;
  case 'S': gradenum = 6;
    break;
  case 'U': gradenum = 7;
    break;
  default:
    printf("Invalid grade.\n");
    return 1;
  }

  switch(grademod) {
    case '+': grademodnum = 0;
	break;
    case '-': grademodnum = 1;
	break;
    case '/': grademodnum = 2;
	break;
    default:
      printf("Invalid grade modifier.\n");
      return 1;
    }

  // credit values
  // last pair first digit 0-5, second digit 0 or 5 (no operation)
  creditval = course[12];
  creditdecimal = course[14];

  switch(creditdecimal) {
    case '0': creddecimal = 0;
	break;
    case '5': creddecimal = 1;
	break;
    default:
      printf("Invalid credit decimal.\n");
      return 1;
    }

  printf("Division: %d\n", divnum);
  printf("Department: %s\n", dept);
  printf("Course: %s\n", coursenum);
  printf("Grade: %d %d\n", gradenum, grademodnum);
  printf("Credits: %c %d\n", creditval, creddecimal);
  return 0;
}
