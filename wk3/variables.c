/*
 Program to demonstrate variable scoping and lifetimes.
 may need to compile with "gcc -lm variables.c" to link the math library
 
 FIXED THE COMPILER ERRORS!
 */

#include <math.h>
#include <stdio.h>

#define ROWS 5
#define COLS 2

void readit(int [][COLS]);
void printChar(int letters[][COLS]);

int global = 6;

double thing(int n)
{
    static int num;    // default init is 0
    double d = (double) n / 100;
	//    d = number;
    d = d + global;
    num = num + 10;
    printf("num is %d\n", num);
	n = num * 10;
	printf("n in thing is %d\n", n);
    return d;
}

int main(void)
{
    int number = 10;
    printf("thing: %f number after thing: %d\n", thing(number), number);
	//    number = n;
    number = global;
    printf("thing: %f\n", thing(number));
    printf("number after thing: %d\n", number);
    
    int values[ROWS][COLS];
    readit(values);
    number = pow(number, 3);
    printf("values[0][0] is %c\n", values[0][0]);
    printChar(values);
    
	thing(values[0][0]);  // passed by value
	printf("values[0][0] is %c\n", values[0][0]);
}

void readit(int ra[][COLS])
{
    printf("enter %d characters\n", ROWS*COLS);
    int r, c;
    for (r=0; r < ROWS; r++)
        for (c=0; c < COLS; c++)
            ra[r][c] = getchar();
    printf("r=%d c=%d\n", r, c);
    printf("first value %c\n", ra[0][0]);
}

void printChar(int ra[][COLS])
{
    //    char ch = ra[0][0];
    for (int r=0; r < ROWS; r++) {
        for (int c=0; c < COLS; c++) // was ROWS
            printf("%c ", ra[r][c]);
        printf("\n");
    }
	//    printf("r=%d c=%d\n", r, c);
}
