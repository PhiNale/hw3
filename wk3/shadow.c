#include <stdio.h>

// a global variable
int v = 100;

// void f();
void f1(void);
void f2(int);

int main(void) {
    // f(3, 5); compiles if the forward declaration were f()
    f1();

    f2(10);

    printf("global v hasn't changed\n");
    printf(" val: %d\n", v);          // we can access the global here
    printf(" loc: %p\n", (void*)&v);  // where global v lives
    printf("size: %lu\n", sizeof(v)); // v has a size (in bytes)
    printf("\n");

    return 0;

}

void f2(int a) {
    printf("a: %d\n", a);
    f1();
}

void f1(void) {
    static int i;
    printf("static i: %d\n", ++i);    // static variable has longer life

    printf("access global v\n");
    printf(" val: %d\n", v);          // we can access the global here
    printf(" loc: %p\n", (void*)&v);  // where global v lives
    printf("size: %lu\n", sizeof(v)); // v has a size (in bytes)
    printf("\n");
 
    // declare a variable in a different scope but with the same name
    double v = 45.0;
    printf("local v 'shadows' global v\n");
    printf(" val: %f\n", v);          // local v "shadows" global v
    printf(" loc: %p\n", (void*)&v);  // local v has its own address
    printf("size: %lu\n", sizeof(v)); // and size
    printf("\n");
}
