/* Homework 1a, 600.120 Spring 2015
Feb. 7
Phillip Espinoza
phil.espinoza@jhu.edu
pespino5

202-251-5121

*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define READSIZE 2
#define MAXDATASIZE 2500

void convert();
void gpa(unsigned data[], int entries);

// return int to allow immediate function termination upon error
int deptlist(unsigned data[], int entries);
int gradelist();
int creditlist();

int main()
{

  // add weight to gpa


  // open courseInts.txt
  FILE * filein = fopen("courseInts.txt", "r");
  if(!filein) {
    fputs("Input file error.\n", stderr);
    return 1;
  }


  // get data from courseInts.txt, store in unsigned int array
  unsigned data[MAXDATASIZE];
  int entries = 0;
  while(fscanf(filein, "%u", &data[entries]) != EOF) {
    entries++;
  }

  /* BEGIN USER INPUT LOOP */

  /* store choice in int to account for possible -1 from
     getchar()

     while condition used to exit on user quit
   */
  int choice = 1;
  while(choice > 0) {

    // menu display
    printf("n - display the total number of courses\n");
    printf("d - list all courses from a particular department\n");
    printf("l - list all courses with a particular letter grade\n");
    printf("c - list all courses with at least a specified number of credits\n");
    printf("g - compute the GPA of all the courses with letter grades\n");
    printf("q - quit the program\n");
    printf("Enter letter choice (single character, case sensitive)-> ");

    // get input from user, discard newline char
    // assumes input format: one char then enter key
    choice = getchar();
    char ch;
  while((ch = getchar()) != '\n' && ch != EOF); // get rid of dangling newline from enter key

    switch(choice) {
    case 'n': printf("Total number of courses: %d.\n", entries);
      break;
    case 'd': if(deptlist(data, entries) != 0) {
	continue;
      }
      break;
    case 'l': if(gradelist(data, entries) != 0) {
	continue;
      }
      break;
    case 'c': if(creditlist(data, entries) != 0) {
	continue;
      }
     break;
    case 'g': gpa(data, entries);
      break;
    case 'q': printf("Quit program.\n");
      choice = -1;
      break;
    default:
      fputs("Invalid choice.\n", stderr);
      break;
    }


    }

  /* END USER INPUT LOOP */

  fclose(filein);

  return 0;
}

/*

Course Integer format: 32-bits
- XXXXXXXXXXXXXXXXXXXXXXXGGGggXXXX
- bits 7-9 (right to left): grade
- bits 5-6 ": grade modifier

grading scale used:
A+: 4.0
A: 4.0
A-: 3.7
B+: 3.3
B: 3.0
B-: 2.7
C+: 2.3
C: 2.0
C-: 1.7
D+: 1.3
D: 1.0
F: 0.0

 */

void gpa(unsigned data[], int entries) {

  double gpapoints = 0; // running total points 
  double creditpoints = 0; // running total for credits
  unsigned gradeletter;
  unsigned grademod;
  unsigned credits;
  unsigned fractions;
  double fractiondouble = 0;

  // begin gpapoints loop

  for (int i = 0; i < entries; i++) {

    // process course integer to isolate grade letter
    gradeletter = data[i];
    credits = data[i];
    fractions = data[i];

    gradeletter = gradeletter >> 6;
    gradeletter = gradeletter & 7; // 7 = 0b111, isolate bits

    credits = credits >> 1;
    credits = credits & 7;

    fractions = fractions & 1;

    // if invalid course, skip to next course
    if (gradeletter == 5
	|| gradeletter == 6
	|| gradeletter == 7) {
      continue;
    }

    // F counts as course but does not affect gpapoints
    if (gradeletter == 4) {
      continue;
    }

    if(fractions == 1) {
      fractiondouble = 0.5;
    }

    creditpoints += credits;
    creditpoints += fractiondouble;


    // process course integer to isolate grade modifier
    grademod = data[i];
    grademod = grademod >> 4;
    grademod = grademod & 3; // 3 = 0b11, isolate bits

    /* grade: switch
       gradeletter determines ones place (no F)
       (add x points)
       grademod determines decimal (if)
       (add or subtract x points)
       update gpapoints
    */
    switch(gradeletter) {
    case 0: gpapoints += (4*credits);
      if(grademod == 1) { // grademod = 1 for minus
	gpapoints -= (.3*credits);
      }
      break;
    case 1: gpapoints += (3*credits);
      if(grademod == 0) { // grademod = 0 for plus
	gpapoints += (.3*credits);
      }
      else if(grademod == 1) {
	gpapoints -= (.3*credits);
      }
      break;
    case 2: gpapoints += (2*credits);
      if(grademod == 0) {
	gpapoints += (.3*credits);
      }
      else if(grademod == 1) {
	gpapoints -= (.3*credits);
      }
      break;
    case 3: gpapoints += (1*credits);
  
      if(grademod == 0) {
	gpapoints += (.3*credits);
      }
      break;
    default: fputs("Error: invalid grade.\n", stderr);
      break;
    }

  }

  // end gpapoints loop

  // because gpapoints is double, integer division will not occur
  double finalgpa = gpapoints/creditpoints;

  printf("Total cumulative GPA: %f.\n", finalgpa);

}

/*

Course Integer format: 32-bits
- XXXXXXXXXXXXXXXXXXXXXXXXXXXXCCCc
- bits 1-4 (right to left): credits
- &15

 */
int creditlist(unsigned data[], int entries) {

  char ch;
  char input[4]; // data must strictly be 3 integers; leave room for '\0'
  unsigned entry;
  unsigned credit;
  unsigned creddecimal;
  unsigned query = 0;

  printf("Please input credits in following format: [0-5].[0, 5].\n");
  if(fgets(input, sizeof(input), stdin) == NULL) {
    fputs("Input error.\n", stderr);
    return 1;
  }
  while((ch = getchar()) != '\n' && ch != EOF); // get rid of dangling newline from enter key

  char credchar = input[0];
  char decimalchar = input[2]; // ignore decimal in input[1]

  switch(credchar) {
  case '0': credit = 0;
    break;
  case '1': credit = 1;
    break;
  case '2': credit = 2;
    break;
  case '3': credit = 3;
    break;
  case '4': credit = 4;
    break;
  case '5': credit = 5;
    break;
  default: fputs("Error: invalid credits.\n", stderr);
    return 1;
  }

  query = query | credit; // add letter code to query
  query = query << 1; // shift to make room for modifier bit

  switch(decimalchar) {
  case '0': creddecimal = 0;
    break;
  case '5': creddecimal = 1;
    break;
  default: fputs("Error: invalid credits.\n", stderr);
    return 1;
  }

  query = query | creddecimal;

  for(int i = 0; i < entries; i++) {
    entry = data[i];
    entry = entry & 15; // 15 = 0b1111
    if(entry != query) { // skip if not target grade
      continue;
    }
    convert(data[i]); // convert() converts and displays
  }

  return 0;
}

/*

Course Integer format: 32-bits
- XXXXXXXXXXXXXXXXXXXXXXXGGGggXXXX
- bits 5-9 (right to left): grade
- >> 4

 */
int gradelist(unsigned data[], int entries) {

  char ch;
  char input[3]; // data assumed to be formatted correct
  unsigned entry;
  unsigned grade;
  unsigned grademod;
  unsigned query = 0;

  printf("Please input grade in following format: [A-F, I, S, U][+, -, /].\n");
  if(fgets(input, sizeof(input), stdin) == NULL) {
    fputs("Input error.\n", stderr);
    return 1;
  }
  while((ch = getchar()) != '\n' && ch != EOF); // get rid of dangling newline from enter key
  input[2] = '\0';

  char gradechar = input[0];
  char grademodchar = input[1];

  switch(gradechar) {
  case 'A': grade = 0;
    break;
  case 'B': grade = 1;
    break;
  case 'C': grade = 2;
    break;
  case 'D': grade = 3;
    break;
  case 'F': grade = 4;
    break;
  case 'I': grade = 5;
    break;
  case 'S': grade = 6;
    break;
  case 'U': grade = 7;
    break;
  default: fputs("Error: invalid grade.\n", stderr);
    return 1;
  }

  query = query | grade; // add letter code to query
  query = query << 2; // shift to make room for modifier bit

  switch(grademodchar) {
  case '+': grademod = 0;
    break;
  case '-': grademod = 1;
    break;
  case '/': grademod = 2;
    break;
  default: fputs("Error: invalid grade modifier.\n", stderr);
    return 1;
  }

  query = query | grademod; // add modifier code to grade code to be searched

  for(int i = 0; i < entries; i++) {
    entry = data[i];
    entry = entry >> 4;
    entry = entry & 31; // 31 = 0b11111
    if(entry != query) { // skip if not target grade
      continue;
    }
    convert(data[i]); // convert() converts and displays
  }

  return 0;

}

/*
  Department bits:
  XXXDDDDDDDDDDXXXXXXXXXXXXXXXXXXX
  - bits 19-29 (right to left) = department code
 */
int deptlist(unsigned data[], int entries) {

  char ch;
  char input[4]; // input assumed to be correct
  unsigned query;
  unsigned dept;

  printf("Please choose department number in following format: (000-999): ");
  if(fgets(input, sizeof(input), stdin) == NULL) {
    fputs("Error.\n", stderr);
  }
  while((ch = getchar()) != '\n' && ch != EOF); // get rid of dangling newline from enter key
  input[3] = '\0';

  query = atoi(input);
  for(int i = 0; i < entries; i++) {
    dept = data[i];
    dept = dept >> 19;
    dept = dept & 1023; // 1023 = 0b1111111111
    if(dept != query) { // skip if not target dept
      continue;
    }
    convert(data[i]); // convert() converts and displays
  }

  return 0;

}

/*

Course integer format:
-DDDddddddddddCCCCCCCCCCGGGggcccf
-D = division
-d = department
-C = course
-G = grade letter
-g = grade modifier
-c = credits
-f = credit fraction

 */
void convert(unsigned courseint) {

  // switches
  // print div string
  // .
  // print dept code (raw)
  // .
  // print course code (raw)
  // print grade char
  // .
  // print grade mod
  // print credit (raw)
  // .
  // print creddec equivalent
  // \n


  unsigned div;
  unsigned dept;
  unsigned course;
  unsigned grade;
  unsigned grademod;
  unsigned credit;
  unsigned creddecimal;

  char divstring[] = "XX";

  char gradechar = 'X';
  char grademodchar = 'X';
  char decimalchar = 'X';

  div = courseint >> 29;
  dept = courseint >> 19;
  course = courseint >> 9;
  grade = courseint >> 6;
  grademod = courseint >> 4;
  credit = courseint >> 1;

  div = div & 7;
  dept = dept & 1023;
  course = course & 1023;
  grade = grade & 7;
  grademod = grademod & 3;
  credit = credit & 7;
  creddecimal = courseint & 1;

  switch(div) {
  case 0: divstring[0] = 'A';
    divstring[1] = 'S';
    break;
  case 1: divstring[0] = 'B';
    divstring[1] = 'U';
    break;
  case 2: divstring[0] = 'E';
    divstring[1] = 'D';
    break;
  case 3: divstring[0] = 'E';
    divstring[1] = 'N';
    break;
  case 4: divstring[0] = 'M';
    divstring[1] = 'E';
    break;
  case 5: divstring[0] = 'P';
    divstring[1] = 'H';
    break;
  case 6: divstring[0] = 'P';
    divstring[1] = 'Y';
    break;
  case 7: divstring[0] = 'S';
    divstring[1] = 'A';
    break;
  default: fputs("CourseInt format error.\n", stderr);
    break;
  }

  switch(grade) {
  case 0: gradechar = 'A';
    break;
  case 1: gradechar = 'B';
    break;
  case 2: gradechar = 'C';
    break;
  case 3: gradechar = 'D';
    break;
  case 4: gradechar = 'F';
    break;
  case 5: gradechar = 'I';
    break;
  case 6: gradechar = 'S';
    break;
  case 7: gradechar = 'U';
    break;
  default: fputs("CourseInt format error.\n", stderr);
    break;
  }

  switch(grademod) {
  case 0: grademodchar = '+';
    break;
  case 1: grademodchar = '-';
    break;
  case 2: grademodchar = '/';
    break;
  default: fputs("CourseInt format error.\n", stderr);
    break;
  }

  switch(creddecimal) {
  case 0: decimalchar = '0';
    break;
  case 1: decimalchar = '5';
    break;
  default: fputs("CourseInt format error.\n", stderr);
    break;
  }

  printf("%s.%u.%u%c%c%u.%c\n", divstring, dept, course, gradechar, grademodchar, credit, decimalchar);

}
