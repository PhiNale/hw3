/* Very short program illustrating scanf and writing to a file.
 */

#include <stdio.h>
#include <assert.h>

int main()
{
    
  FILE * file = fopen("output.txt", "w"); // creates a file in the same dir as executable
   
  // Make sure fopen worked
  assert(file); // file == 0 is false is failure
    
  printf("What is your name?\n");
    
  char name[32];
  scanf("%s", name);
    
  printf("What is your age?\n");
    
  int age;
  scanf("%d", &age); // & needed since without it no value would show up in age (call-by-value)
    
  // Alternative to the above -- scanf can read multiple inputs
  //     printf("What is your name and age?\n");
  //     scanf("%s %d", name, &age);

    
  if ( age >= 21 )
    fprintf(file, "Hello %s, you are old enough to drink\n", name); // write to output.txt
  else
    fprintf(file, "Hello %s, you are NOT old enough to drink\n", name);
    
  fclose(file);
}
