/* Homework 1a, 600.120 Spring 2015

Feb. 7
Phillip Espinoza
phil.espinoza@jhu.edu
pespino5

202-251-5121

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define DIVLENGTH 2
#define LENGTH 17

int main(int argc, char* argv[]) {

  // Quit if input filename not provided
  if(argc < 2) {
    fputs("Input filename required.\n", stderr);
    return 1;
  }

  // data storage variables
  // atoi()
  char divcode[] = "XX";
  char dept[] = "XXX";
  char coursenum[] = "XXX";
  // c - '0'
  char grade = 'X';
  char grademod = 'X';
  char creditval = 'X';
  char creditdecimal = 'X';
  int divnum, gradenum, grademodnum,creddecimal;
  char course[LENGTH];

  // code adapted from course website
  FILE * filein = fopen(argv[1], "r");
  FILE * fileout = fopen("courseInts.txt", "w");
  assert(filein);
  assert(fileout);


  if(!filein) {
    fputs("Input file error.\n", stderr);
    return 1;
  }

  while(fgets(course, LENGTH, filein) != NULL) {

    // first 3 course department code (switch)
    strncpy(divcode, course, DIVLENGTH);

    divcode[DIVLENGTH+1] = '\0';

    if (strcmp(divcode, "AS") == 0) {
      divnum = 0;
    }
    else if (strcmp(divcode, "BU") == 0) {
      divnum = 1;
    }
    else if (strcmp(divcode, "ED") == 0) {
      divnum = 2;
    }
    else if (strcmp(divcode, "EN") == 0) {
      divnum = 3;
    }
    else if (strcmp(divcode, "ME") == 0) {
      divnum = 4;
    }
    else if (strcmp(divcode, "PH") == 0) {
      divnum = 5;
    }
    else if (strcmp(divcode, "PY") == 0) {
      divnum = 6;
    }
    else if (strcmp(divcode, "SA") == 0) {
      divnum = 7;
    }
    else {
      fputs("Invalid division code.\n", stderr);
      return 1;
    }

    // department number
    dept[0] = course[3];
    dept[1] = course[4];
    dept[2] = course[5];

    // course number
    coursenum[0] = course[7];
    coursenum[1] = course[8];
    coursenum[2] = course[9];

    // integer equivalent grade values
    grade = course[10];
    grademod = course[11];

    switch(grade) {
    case 'A': gradenum = 0;
      break;
    case 'B': gradenum = 1;
      break;
    case 'C': gradenum = 2;
      break;
    case 'D': gradenum = 3;
      break;
    case 'F': gradenum = 4;
      break;
    case 'I': gradenum = 5;
      break;
    case 'S': gradenum = 6;
      break;
    case 'U': gradenum = 7;
      break;
    default:
      fputs("Invalid grade.\n", stderr);
      return 1;
    }

    switch(grademod) {
    case '+': grademodnum = 0;
      break;
    case '-': grademodnum = 1;
      break;
    case '/': grademodnum = 2;
      break;
    default:
      fputs("Invalid grade modifier.\n", stderr);
      return 1;
    }

    // credit values
    // last pair first digit 0-5, second digit 0 or 5
    creditval = course[12];
    creditdecimal = course[14];

    switch(creditdecimal) {
    case '0': creddecimal = 0;
      break;
    case '5': creddecimal = 1;
      break;
    default:
      fputs("Invalid credit fraction.\n", stderr);
      return 1;
    }


    // data in char format must be converted to integers
    // atoi()
    int deptinteger = atoi(dept);
    int courseinteger = atoi(coursenum);
    // c- '0'
    int creditvalinteger = creditval - '0';

    // division code: 0-7 = 0bXXX
    // dept code, course number = 0bXXXXXXXXXX (<< 10 twice)
    // grade code: 0-7 = 0bXXX (<< 3)
    // grade mod = 0bXX (<< 2)
    // credits: 0-5 = 0bXXX (<< 3)
    // credit decimal: 0, 5 = 0bX  (<< 1)

    unsigned int infocomplete = divnum;

    infocomplete = infocomplete << 10;
    infocomplete = infocomplete | deptinteger;

    infocomplete = infocomplete << 10;
    infocomplete = infocomplete | courseinteger;

    infocomplete = infocomplete << 3;
    infocomplete = infocomplete | gradenum;

    infocomplete = infocomplete << 2;
    infocomplete = infocomplete | grademodnum;

    infocomplete = infocomplete << 3;
    infocomplete = infocomplete | creditvalinteger;

    infocomplete = infocomplete << 1;
    infocomplete = infocomplete | creddecimal;

    fprintf(fileout, "%u\n", infocomplete);

    course[LENGTH-2] = '\0'; // remove \n from fgets()
    printf("%s  %u  ", course, infocomplete);

    // binary conversion
    for (int i = 31; i >= 0; i--) {
      int currentplace = infocomplete >> i;

      if (currentplace & 1) {
	printf("1");
      }
      else {
	printf("0");
      }

    }
    printf("\n");  
  }

  fclose(filein);
  fclose(fileout);

  return 0;

}
