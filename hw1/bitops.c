/* BIT OPERATIONS

& bit-wise and, used to mask
  01011
& 10110
---------
  00010

| bit-wise or
  01011
| 10110
---------
  11111

~ 1001 == 0110 complement operator

Left Shift
0000110010 << 3   ==  0110010000  (multiply by 8=2^3)
0000001  <<  1  == 0000010

Right Shift
0000110010 >> 3   ==  ???0000110 (divide by 8)
???0000110  & 0b0001111111  == 0000000110

*/

#include <stdio.h>
#include <assert.h>
#include <math.h>

int main(void) {

  unsigned short val = 6;
  unsigned short num = 3;

  assert((val & num) == 2);
  assert((val | num) == 7);
  assert((val ^ num) == 5);

  printf("~6 in bit ops is %d\n", ~val);
  printf("size of short is %ld\n", sizeof(short));  // # bytes

  assert((val << 3) == val * pow(2,3));
  val = val << 3;
  assert((val >> 3) == val / 8);


  printf("all tests passed! yay!\n");

  int i = 24;
  printf("sizeof(int) == %ld", sizeof(i));
}
