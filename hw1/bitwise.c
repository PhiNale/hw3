#include <stdio.h>
#include <assert.h>


int main(int argc, char* argv[]) {

  if(argc < 2) {
    fputs("Input filename required.\n", stderr);
    return 1;
  }

  unsigned short five = 5;
  unsigned short three = 3;
  unsigned short seven = 7;
  unsigned short two = 2;

  assert(argc < 10);
  assert(five < 10);
  assert(seven < 10);
  assert(two < 10);

  // argv[n] testing

  printf("five = %d\n", five);
  printf("three = %d\n", three);
  printf("seven = %d\n", seven);
  printf("two = %d\n", two);
  printf("argc = %d\n", argc);
  printf("Argument [1]: %s\n", argv[1]);


  return 0;

}
